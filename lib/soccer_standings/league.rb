module SoccerStandings
  # Number of points a team receives towards their standings score for a win
  WIN_POINTS = 3
  
  # Number of points a team receives towards their standings score for a tie
  TIE_POINTS = 1
  
  class League
    # Sorts teams by league standings
    #
    # param [Hash] teams hash containing Team objects, keyed by team names
    # return [Array] array containing Team objects sorted by league standings
    def self.sort_teams_by_standings(teams)
      calculate_team_standings(teams)
      teams.values.sort do |a, b|
        comparison = (b.score <=> a.score)
        comparison.zero? ? (a.name <=> b.name) : comparison
      end
    end

    # Calculates team standings based on number of wins and ties
    #
    # param [Hash] teams hash containing Team objects, keyed by team names
    def self.calculate_team_standings(teams)
      teams.each do |team_name, team|
        team.score = (team.wins * WIN_POINTS) + (team.ties * TIE_POINTS)
      end
    end
  end
end
