module SoccerStandings
  class Application
    def run
      # Process each line of input and push game objects into an array.
      # Each game object contains both teams' names and scores.
      games = []

      ARGF.each do |game_score|
        games.push(Game.new(game_score))
      end

      # Generate an hash containing each team that played in provided array of games
      teams = Team::from_game_array(games)
      
      League::sort_teams_by_standings(teams).each do |team|
        puts team.name << ", " << team.score.to_s << (team.score == 1 ? " pt" : " pts")
      end
    end
  end
end
