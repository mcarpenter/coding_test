$LOAD_PATH.unshift(File.join(File.dirname(__FILE__), '..', 'lib'))
$LOAD_PATH.unshift(File.dirname(__FILE__))

require 'soccer_standings/application.rb'
require 'soccer_standings/game.rb'
require 'soccer_standings/league.rb'
require 'soccer_standings/team.rb'
