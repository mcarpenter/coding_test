require 'spec_helper.rb'
module SoccerStandings
  describe Team do
    before :each do
      @games = [Game.new("Seahawks 58, Cardinals 0"),
                Game.new("49ers 31, Seahawks 42"),
                Game.new("Seahawks 24, Patriots 23"),
                Game.new("Rams 19, Seahawks 13"),
                Game.new("49ers 3, Rams 3")]
      @teams = Team::from_game_array(@games)
      
      @new_team = Team.new("Team")
    end
    
    describe "#record_win" do
      it "Should add 1 win" do
        @new_team.record_win
        @new_team.wins.should eql 1
      end
    end
    
    describe "#record_Tie" do
      it "Should add 1 tie" do
        @new_team.record_tie
        @new_team.ties.should eql 1
      end
    end

    describe "#frome_game_array" do
      it "should return a hash of all teams represented in the array of games" do
        @teams.should include "Seahawks"
        @teams.should include "Cardinals"
        @teams.should include "49ers"
        @teams.should include "Patriots"
        @teams.should include "Rams"
      end

      it "should not contain any duplicate teams" do
        new_hash = @teams.values.uniq { |team| team.name }
        new_hash.length.should eql @teams.length
      end

      it "should record wins" do
        @teams['Seahawks'].wins.should eql 3
      end

      it "should record ties" do
        @teams['49ers'].ties.should eql 1
        @teams['Rams'].ties.should eql 1
      end
    end
  end
end
